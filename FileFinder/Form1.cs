﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FileFinder.Properties;

namespace FileFinder
{

    public partial class MainForm : Form
    {
        public static DateTime RunTime = new DateTime();

        public MainForm()
        {
            InitializeComponent();

            //Заполняем последние настройки
            txtDirectoryPath.Text = Settings.Default.lastPath;
            txtFilterFileName.Text = Settings.Default.lastFilterName;
            txtTextInFile.Text = Settings.Default.lastTextInFile;
        }

        private void btnSelectDirectory_Click(object sender, EventArgs e)
        {
            var fbd = new FolderBrowserDialog
            {
                ShowNewFolderButton = false
            };

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtDirectoryPath.Text = fbd.SelectedPath;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.lastPath = txtDirectoryPath.Text;
            Settings.Default.lastFilterName = txtFilterFileName.Text;
            Settings.Default.lastTextInFile = txtTextInFile.Text;
            Settings.Default.Save();
        }

        private void RunTime_Tick(object sender, EventArgs e)
        {
            RunTime = RunTime.AddSeconds(1);
            lblRunTime.Text = RunTime.ToString("mm:ss");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (RunTime != DateTime.MinValue)
            {
                tmrRunTime.Start();
            }
            else
            {
                RunTime = new DateTime();
                lblRunTime.Text = "00:00";
                tmrRunTime.Start();
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            if (tmrRunTime.Enabled)
            {
                tmrRunTime.Stop();
                btnStart.Text = "Продолжить";
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            tmrRunTime.Stop();
            lblRunTime.Text = "00:00";
            btnStart.Text = "Найти";
            RunTime = new DateTime();
        }
    }
}
