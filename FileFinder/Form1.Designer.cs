﻿namespace FileFinder
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FiltersBox = new System.Windows.Forms.GroupBox();
            this.lblDirectoryPath = new System.Windows.Forms.Label();
            this.txtTextInFile = new System.Windows.Forms.TextBox();
            this.lblTextInFile = new System.Windows.Forms.Label();
            this.lblFilterFileName = new System.Windows.Forms.Label();
            this.txtFilterFileName = new System.Windows.Forms.TextBox();
            this.btnSelectDirectory = new System.Windows.Forms.Button();
            this.txtDirectoryPath = new System.Windows.Forms.TextBox();
            this.treeViewFiles = new System.Windows.Forms.TreeView();
            this.lblCurrentFile = new System.Windows.Forms.Label();
            this.tmrRunTime = new System.Windows.Forms.Timer(this.components);
            this.btnStart = new System.Windows.Forms.Button();
            this.lblRunTime = new System.Windows.Forms.Label();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.FiltersBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // FiltersBox
            // 
            this.FiltersBox.Controls.Add(this.lblDirectoryPath);
            this.FiltersBox.Controls.Add(this.txtTextInFile);
            this.FiltersBox.Controls.Add(this.lblTextInFile);
            this.FiltersBox.Controls.Add(this.lblFilterFileName);
            this.FiltersBox.Controls.Add(this.txtFilterFileName);
            this.FiltersBox.Controls.Add(this.btnSelectDirectory);
            this.FiltersBox.Controls.Add(this.txtDirectoryPath);
            this.FiltersBox.Location = new System.Drawing.Point(541, 12);
            this.FiltersBox.Name = "FiltersBox";
            this.FiltersBox.Size = new System.Drawing.Size(247, 159);
            this.FiltersBox.TabIndex = 0;
            this.FiltersBox.TabStop = false;
            this.FiltersBox.Text = "Критерии поиска";
            // 
            // lblDirectoryPath
            // 
            this.lblDirectoryPath.AutoSize = true;
            this.lblDirectoryPath.Location = new System.Drawing.Point(6, 29);
            this.lblDirectoryPath.Name = "lblDirectoryPath";
            this.lblDirectoryPath.Size = new System.Drawing.Size(122, 13);
            this.lblDirectoryPath.TabIndex = 6;
            this.lblDirectoryPath.Text = "Стартовая директория";
            // 
            // txtTextInFile
            // 
            this.txtTextInFile.BackColor = System.Drawing.SystemColors.Control;
            this.txtTextInFile.Location = new System.Drawing.Point(90, 126);
            this.txtTextInFile.Name = "txtTextInFile";
            this.txtTextInFile.Size = new System.Drawing.Size(146, 20);
            this.txtTextInFile.TabIndex = 5;
            // 
            // lblTextInFile
            // 
            this.lblTextInFile.AutoSize = true;
            this.lblTextInFile.Location = new System.Drawing.Point(6, 129);
            this.lblTextInFile.Name = "lblTextInFile";
            this.lblTextInFile.Size = new System.Drawing.Size(84, 13);
            this.lblTextInFile.TabIndex = 4;
            this.lblTextInFile.Text = "Текст в файле:";
            // 
            // lblFilterFileName
            // 
            this.lblFilterFileName.AutoSize = true;
            this.lblFilterFileName.Location = new System.Drawing.Point(6, 103);
            this.lblFilterFileName.Name = "lblFilterFileName";
            this.lblFilterFileName.Size = new System.Drawing.Size(119, 13);
            this.lblFilterFileName.TabIndex = 3;
            this.lblFilterFileName.Text = "Шаблон имени файла:";
            // 
            // txtFilterFileName
            // 
            this.txtFilterFileName.BackColor = System.Drawing.SystemColors.Control;
            this.txtFilterFileName.Location = new System.Drawing.Point(131, 100);
            this.txtFilterFileName.Name = "txtFilterFileName";
            this.txtFilterFileName.Size = new System.Drawing.Size(105, 20);
            this.txtFilterFileName.TabIndex = 2;
            // 
            // btnSelectDirectory
            // 
            this.btnSelectDirectory.Location = new System.Drawing.Point(161, 71);
            this.btnSelectDirectory.Name = "btnSelectDirectory";
            this.btnSelectDirectory.Size = new System.Drawing.Size(75, 23);
            this.btnSelectDirectory.TabIndex = 1;
            this.btnSelectDirectory.Text = "Выбрать";
            this.btnSelectDirectory.UseVisualStyleBackColor = true;
            this.btnSelectDirectory.Click += new System.EventHandler(this.btnSelectDirectory_Click);
            // 
            // txtDirectoryPath
            // 
            this.txtDirectoryPath.BackColor = System.Drawing.SystemColors.Control;
            this.txtDirectoryPath.Location = new System.Drawing.Point(6, 45);
            this.txtDirectoryPath.Name = "txtDirectoryPath";
            this.txtDirectoryPath.Size = new System.Drawing.Size(230, 20);
            this.txtDirectoryPath.TabIndex = 0;
            // 
            // treeViewFiles
            // 
            this.treeViewFiles.Location = new System.Drawing.Point(12, 12);
            this.treeViewFiles.Name = "treeViewFiles";
            this.treeViewFiles.Size = new System.Drawing.Size(523, 413);
            this.treeViewFiles.TabIndex = 1;
            // 
            // lblCurrentFile
            // 
            this.lblCurrentFile.AutoSize = true;
            this.lblCurrentFile.Location = new System.Drawing.Point(12, 428);
            this.lblCurrentFile.Name = "lblCurrentFile";
            this.lblCurrentFile.Size = new System.Drawing.Size(120, 13);
            this.lblCurrentFile.TabIndex = 2;
            this.lblCurrentFile.Text = "Текущий файл поиска";
            // 
            // tmrRunTime
            // 
            this.tmrRunTime.Interval = 1000;
            this.tmrRunTime.Tick += new System.EventHandler(this.RunTime_Tick);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(693, 177);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(84, 31);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Найти";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblRunTime
            // 
            this.lblRunTime.AutoSize = true;
            this.lblRunTime.Location = new System.Drawing.Point(607, 186);
            this.lblRunTime.Name = "lblRunTime";
            this.lblRunTime.Size = new System.Drawing.Size(34, 13);
            this.lblRunTime.TabIndex = 4;
            this.lblRunTime.Text = "00:00";
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(621, 231);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 5;
            this.btnPause.Text = "Пауза";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(702, 231);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Стоп";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.lblRunTime);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblCurrentFile);
            this.Controls.Add(this.treeViewFiles);
            this.Controls.Add(this.FiltersBox);
            this.Name = "MainForm";
            this.Text = "Поиск файлов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FiltersBox.ResumeLayout(false);
            this.FiltersBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox FiltersBox;
        private System.Windows.Forms.Button btnSelectDirectory;
        private System.Windows.Forms.TextBox txtDirectoryPath;
        private System.Windows.Forms.Label lblFilterFileName;
        private System.Windows.Forms.TextBox txtFilterFileName;
        private System.Windows.Forms.Label lblTextInFile;
        private System.Windows.Forms.TreeView treeViewFiles;
        private System.Windows.Forms.Label lblCurrentFile;
        private System.Windows.Forms.Label lblDirectoryPath;
        private System.Windows.Forms.TextBox txtTextInFile;
        private System.Windows.Forms.Timer tmrRunTime;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblRunTime;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStop;
    }
}

